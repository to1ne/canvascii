# Canvascii

gRPC server-client example which allows you to draw ASCII chars on a text
canvas.

## Usage

It's recommended to run the commands using the `Makefile`. By default the server
runs on `localhost:5000`. If you want to use another address or port, you can
set it with the environment variable `ADDRESS`. For example:

```sh
ADDRESS=198.128.1.1:4567
```

All client functions allow specifying the canvas name:

```sh
CANVAS=my-canvas
```

### Server

To start the server:

```sh
make serve
```

### Watch

To watch the canvas at real-time, run:

```sh
make watch
```

To see the canvas get punched rune-by-rune, you can specify a timeout between
each rune, for example a 20ms delay use:

```sh
make watch DELAY=20
```

By default the canvas is 80 runes wide, and 40 runes high. You can change this
with the parameters:

- `WIDTH`: The width of the canvas.
- `HEIGHT`: The height of the canvas.

### Drawing

To draw a rectangle:

```sh
make draw-rect
```

It's possible to specify the parameters of the rect with the following variables:

- `X`: The X position of the origin of the rect.
- `Y`: The Y position of the origin of the rect.
- `WIDTH`: The width of the rect.
- `HEIGHT`: The height of the rect.
- `FILL`: The fill character of the rect.
- `OUTLINE`: The outline character of the rect.
  char.

Example:

```sh
make draw-rect X=1 Y=2 WIDTH=3 HEIGHT=4 FILL=+ OUTLINE=#
```

Or to flood fill an area:

```sh
make flood-fill X=1 Y=2 FILL=@
```

`flood-fill` supports the following parameters:

- `X`: The X position of the origin of the flood.
- `Y`: The Y position of the origin of the flood.
- `FILL`: The character fill the flood.

## Development

### Generate gRPC code

To generate gRPC Go code, install these packages:

```sh
go get google.golang.org/protobuf/cmd/protoc-gen-go \
       google.golang.org/grpc/cmd/protoc-gen-go-grpc
```

## License

This project is disctributed under the
[Mozilla Public License Version 2.0](LICENSE).
