package main

import (
	"flag"
	"log"
	"net"
)

func main() {
	address := flag.String("address", "", "The address to run the server on")
	flag.Parse()
	if *address == "" {
		log.Fatalf("no address specified")
	}

	lis, err := net.Listen("tcp", *address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := newGrpcServer()
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
