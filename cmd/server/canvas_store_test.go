package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	pb "gitlab.com/to1ne/canvascii/canvascii"
	"gitlab.com/to1ne/canvascii/internal/testhelper"
)

func TestCanvasStore_Load(t *testing.T) {
	_, name, store := initStore(t, `{"Rects":[{"Origin":{"X":10,"Y":3},"Width":20,"Height":4,"Fill":36,"Outline":37}]}`)

	result, err := store.Load(name)
	require.NoError(t, err, "failed to load file")

	expected := &pb.Canvas{Rects: []*pb.Rect{testhelper.NewRect(10, 3, 20, 4, 36, 37)}}

	testhelper.ProtoEqual(t, expected, result)
}

func TestCanvasStore_Save(t *testing.T) {
	file, name, store := initStore(t, "")

	err := store.Save(name, &pb.Canvas{Rects: []*pb.Rect{testhelper.NewRect(10, 3, 20, 4, 36, 37)}})
	require.NoError(t, err, "failed to load file")

	expected := []byte(`{"Rects":[{"Origin":{"X":10,"Y":3},"Width":20,"Height":4,"Fill":36,"Outline":37}]}`)

	result, err := ioutil.ReadFile(file)
	require.NoError(t, err, "failed to write test file")

	JsonEqual(t, expected, result)
}

func TestCanvasStore_Append(t *testing.T) {
	file, name, store := initStore(t, `{"Operations":[{"Rect":{"Origin":{"X":10,"Y":3},"Width":20,"Height":4,"Fill":36,"Outline":37}}]}`)

	op := &pb.Operation{Op: &pb.Operation_Rect{Rect: testhelper.NewRect(8, 2, 12, 6, 42, 61)}}
	err := store.Append(name, op)
	require.NoError(t, err, "failed to load file")

	rect1 := `{"Origin":{"X":10, "Y":3}, "Width":20, "Height":4, "Fill":36, "Outline":37}`
	rect2 := `{"Origin":{"X":8,"Y":2},"Width":12,"Height":6,"Fill":42,"Outline":61}`
	expected := []byte(fmt.Sprintf(`{"Operations":[{"Rect":%s},{"Rect":%s}]}`, rect1, rect2))

	result, err := ioutil.ReadFile(file)
	require.NoError(t, err, "failed to write test file")

	JsonEqual(t, expected, result)
}

func JsonEqual(t *testing.T, expected, actual []byte) {
	var oe interface{}
	var oa interface{}

	err := json.Unmarshal(expected, &oe)
	require.NoError(t, err, "Error mashalling expected JSON")

	err = json.Unmarshal(actual, &oa)
	require.NoError(t, err, "Error mashalling actual JSON")

	require.True(t, reflect.DeepEqual(oa, oe), "JSON messages not equal\nexpected: %v\ngot:      %v", oe, oa)
}

func initStore(t *testing.T, content string) (string, string, canvasStore) {
	file, err := ioutil.TempFile("", "*.cascii")
	require.NoError(t, err, "failed to create test file")
	t.Cleanup(func() { os.Remove(file.Name()) })

	if len(content) > 0 {
		_, err = file.Write([]byte(content))
		require.NoError(t, err, "failed to write test file")
	}
	file.Close()

	dir, name := path.Split(file.Name())

	return file.Name(), strings.TrimSuffix(name, ".cascii"), canvasStore{dataDir: dir}
}
