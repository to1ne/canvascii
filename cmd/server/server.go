package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"sync"
	"sync/atomic"

	pb "gitlab.com/to1ne/canvascii/canvascii"
	"google.golang.org/grpc"
)

// server implements the API for the Canvascii service.
type server struct {
	pb.UnimplementedCanvasciiServer
	store    canvasStore
	watchers sync.Map
	watchIdx uint64
}

type watcher struct {
	opChan     chan *pb.Operation
	canvasName string
}

func newServer() *server {
	return &server{
		store: NewCanvasStore(),
	}
}

func newGrpcServer() *grpc.Server {
	s := grpc.NewServer()
	pb.RegisterCanvasciiServer(s, newServer())

	return s
}

type hasCanvasName interface {
	GetCanvasName() string
}

func safeGetName(o hasCanvasName) string {
	name := o.GetCanvasName()
	if name != "" {
		return name
	}
	return "default"
}

// DrawRect draws the rect on the canvas
func (s *server) DrawRect(ctx context.Context, r *pb.Rect) (*pb.Rect, error) {
	log.Printf("Received rect %v\n", r)

	op := &pb.Operation{Op: &pb.Operation_Rect{Rect: r}}
	name := safeGetName(r)
	s.store.Append(name, op)
	s.publish(name, op)

	return r, nil
}

func (s *server) FloodFill(ctx context.Context, f *pb.Flood) (*pb.Flood, error) {
	log.Printf("Received flood %v\n", f)

	op := &pb.Operation{Op: &pb.Operation_Flood{Flood: f}}
	name := safeGetName(f)
	s.store.Append(name, op)
	s.publish(name, op)

	return f, nil
}

func (s *server) Watch(req *pb.Hello, stream pb.Canvascii_WatchServer) error {
	name := safeGetName(req)
	if err := s.replay(name, stream); err != nil {
		return fmt.Errorf("failed to replay: %v", err)
	}

	ctx := stream.Context()
	ch, stop := s.subscribe(name)
	defer stop()

	for {
		select {
		case op := <-ch:
			if err := stream.Send(op); err != nil {
				return err
			}
		case <-ctx.Done():
			log.Printf("client has disconnected")
			return nil
		}
	}
}

// replay streams all the data from the canvas saved on disk
func (s *server) replay(name string, stream pb.Canvascii_WatchServer) error {
	canvas, err := s.store.Load(name)
	if err != nil {
		errNotExist := os.ErrNotExist
		if errors.As(err, &errNotExist) {
			return nil
		}
		return fmt.Errorf("could not load initial canvas: %v", err)
	}

	for _, op := range canvas.GetOperations() {
		if err := stream.Send(op); err != nil {
			return err
		}
	}

	return nil
}

func (s *server) subscribe(name string) (chan *pb.Operation, func()) {
	ch := make(chan *pb.Operation)
	k := atomic.AddUint64(&s.watchIdx, 1)
	s.watchers.Store(k, watcher{opChan: ch, canvasName: name})

	return ch, func() { s.unsubscribe(k) }
}

func (s *server) unsubscribe(k uint64) {
	v, ok := s.watchers.LoadAndDelete(k)
	if !ok {
		log.Printf("failed to find watcher with key '%v'\n", k)
		return
	}
	w, ok := v.(watcher)
	if !ok {
		log.Printf("failed to retrieve watcher with key '%v'\n", k)
		return
	}

	close(w.opChan)
}

func (s *server) publish(name string, op *pb.Operation) {
	s.watchers.Range(func(k interface{}, v interface{}) bool {
		w, ok := v.(watcher)
		if !ok {
			log.Printf("failed to cast value with key '%v' into watcher\n", k)
			return true
		}
		if w.canvasName == name {
			w.opChan <- op
		}

		return true
	})
}
