package main

import (
	"fmt"
	"io/ioutil"
	"os"

	pb "gitlab.com/to1ne/canvascii/canvascii"
	"google.golang.org/protobuf/encoding/protojson"
)

type canvasStore struct {
	dataDir string
}

func NewCanvasStore() canvasStore {
	return canvasStore{
		dataDir: "data",
	}
}

func (s *canvasStore) filename(name string) string {
	return fmt.Sprintf("%s/%s.cascii", s.dataDir, name)
}

func (s *canvasStore) Load(name string) (*pb.Canvas, error) {
	data, err := ioutil.ReadFile(s.filename(name))
	if err != nil {
		return nil, fmt.Errorf("failed to read canvas from file: %v", err)
	}
	result := pb.Canvas{}

	err = protojson.Unmarshal(data, &result)
	if err != nil {
		return nil, fmt.Errorf("failed to unmashal canvas: %v", err)
	}

	return &result, nil
}

func (s *canvasStore) Save(name string, c *pb.Canvas) error {
	data, err := protojson.Marshal(c)
	if err != nil {
		return fmt.Errorf("failed to mashal canvas: %v", err)
	}

	err = os.MkdirAll(s.dataDir, 0755)
	if err != nil {
		return fmt.Errorf("failed to create data dir: %v", err)
	}

	err = ioutil.WriteFile(s.filename(name), data, 0644)
	if err != nil {
		return fmt.Errorf("failed to write canvas to file: %v", err)
	}

	return nil
}

// Append adds the given Operation to the canvas file on disk
func (s *canvasStore) Append(name string, op *pb.Operation) error {
	c, err := s.Load(name)
	if err != nil {
		fmt.Errorf("failed to load canvas: %v", err)
	}

	c = &pb.Canvas{
		Operations: append(c.GetOperations(), op),
	}

	err = s.Save(name, c)
	if err != nil {
		fmt.Errorf("failed to save canvas: %v", err)
	}

	return nil
}
