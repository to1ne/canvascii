package main

import (
	"context"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	pb "gitlab.com/to1ne/canvascii/canvascii"
)

func TestServer_DrawRect(t *testing.T) {
	ctx := context.Background()
	s := newServer()
	s.store.dataDir = os.TempDir()
	rect := pb.Rect{}

	_, err := s.DrawRect(ctx, &rect)
	require.NoError(t, err, "DrawRect failed")
}
