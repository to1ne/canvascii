package main

import (
	"context"
	"flag"
	"log"
	"time"

	pb "gitlab.com/to1ne/canvascii/canvascii"
)

type drawRectCmd struct {
	x, y, w, h uint
	f, o       string
}

func (cmd *drawRectCmd) rect() *pb.Rect {
	var f, o rune

	if len(cmd.f) > 0 {
		f = []rune(cmd.f)[0]
	}
	if len(cmd.o) > 0 {
		o = []rune(cmd.o)[0]
	}

	return &pb.Rect{
		Origin: &pb.Coordinate{X: uint32(cmd.x), Y: uint32(cmd.y)},
		Width:  uint32(cmd.w), Height: uint32(cmd.h),
		Fill: uint32(f), Outline: uint32(o),
		CanvasName: canvasName,
	}
}

func (cmd *drawRectCmd) Flags() *flag.FlagSet {
	flags := flag.NewFlagSet("draw-rect", flag.ExitOnError)

	flags.UintVar(&cmd.x, "x", 0, "X coordinate of the origin of rect")
	flags.UintVar(&cmd.y, "y", 0, "Y coordinate of the origin of rect")
	flags.UintVar(&cmd.w, "width", 0, "width of rect")
	flags.UintVar(&cmd.h, "height", 0, "height of rect")
	flags.StringVar(&cmd.f, "fill", "", "fill ASCII character of rect")
	flags.StringVar(&cmd.o, "outline", "", "outline ASCII character of rect")

	return flags
}

func (cmd *drawRectCmd) Run(client pb.CanvasciiClient) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	req, err := client.DrawRect(ctx, cmd.rect())
	if err != nil {
		log.Fatalf("failed to draw rect: %v", err)
	}
	log.Printf("Successfully drawn rect: %v\n", req)

	return nil
}
