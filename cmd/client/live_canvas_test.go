package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	pb "gitlab.com/to1ne/canvascii/canvascii"
	"gitlab.com/to1ne/canvascii/internal/testhelper"
)

func TestCanvas(t *testing.T) {
	testCases := []struct {
		desc          string
		width, height int
		draw          func(LiveCanvas)
		expected      []string
	}{
		{
			desc:  "empty canvas",
			width: 32, height: 12,
			draw: func(LiveCanvas) {},
			expected: []string{
				"                                ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
			},
		},
		{
			desc:  "simple rect",
			width: 32, height: 12,
			draw: func(c LiveCanvas) {
				c.DrawRect(testhelper.NewRect(3, 2, 10, 6, 42, 35))
			},
			expected: []string{
				"                                ",
				"                                ",
				"   ##########                   ",
				"   #********#                   ",
				"   #********#                   ",
				"   #********#                   ",
				"   #********#                   ",
				"   ##########                   ",
				"                                ",
				"                                ",
				"                                ",
				"                                ",
			},
		},
		{
			desc:  "fixture 1",
			width: 24, height: 9,
			draw: func(c LiveCanvas) {
				c.DrawRect(testhelper.NewRect(3, 2, 5, 3, 88, 64))
				c.DrawRect(testhelper.NewRect(10, 3, 14, 6, 79, 88))
			},
			expected: []string{
				"                        ",
				"                        ",
				"   @@@@@                ",
				"   @XXX@  XXXXXXXXXXXXXX",
				"   @@@@@  XOOOOOOOOOOOOX",
				"          XOOOOOOOOOOOOX",
				"          XOOOOOOOOOOOOX",
				"          XOOOOOOOOOOOOX",
				"          XXXXXXXXXXXXXX",
			},
		},
		{
			desc:  "fixture 2",
			width: 21, height: 8,
			draw: func(c LiveCanvas) {
				c.DrawRect(testhelper.NewRect(14, 0, 7, 6, 46, 0))
				c.DrawRect(testhelper.NewRect(0, 3, 8, 4, 0, 79))
				c.DrawRect(testhelper.NewRect(5, 5, 5, 3, 88, 88))
			},
			expected: []string{
				"              .......",
				"              .......",
				"              .......",
				"OOOOOOOO      .......",
				"O      O      .......",
				"O    XXXXX    .......",
				"OOOOOXXXXX           ",
				"     XXXXX           ",
			},
		},
		{
			desc:  "fixture 3",
			width: 21, height: 8,
			draw: func(c LiveCanvas) {
				c.DrawRect(testhelper.NewRect(14, 0, 7, 6, 46, 0))
				c.DrawRect(testhelper.NewRect(0, 3, 8, 4, 0, 79))
				c.DrawRect(testhelper.NewRect(5, 5, 5, 3, 88, 88))
				c.DrawFlood(&pb.Flood{Origin: &pb.Coordinate{X: 0, Y: 0}, Fill: 45})
			},
			expected: []string{
				"--------------.......",
				"--------------.......",
				"--------------.......",
				"OOOOOOOO------.......",
				"O      O------.......",
				"O    XXXXX----.......",
				"OOOOOXXXXX-----------",
				"     XXXXX-----------",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			canvas := NewLiveCanvas(nil, tc.width, tc.height, 0)
			canvas.output = nil

			tc.draw(canvas)

			for i, row := range canvas.canvas {
				require.Equalf(t, tc.expected[i], string(row[:]), "Mismatch on row %d", i)
			}
		})
	}
}
