package main

import (
	"context"
	"flag"
	"log"
	"time"

	pb "gitlab.com/to1ne/canvascii/canvascii"
)

type floodFillCmd struct {
	x, y uint
	f    string
}

func (cmd *floodFillCmd) flood() *pb.Flood {
	var f rune

	if len(cmd.f) > 0 {
		f = []rune(cmd.f)[0]
	}

	return &pb.Flood{
		Origin:     &pb.Coordinate{X: uint32(cmd.x), Y: uint32(cmd.y)},
		Fill:       uint32(f),
		CanvasName: canvasName,
	}
}

func (cmd *floodFillCmd) Flags() *flag.FlagSet {
	flags := flag.NewFlagSet("flood-fill", flag.ExitOnError)

	flags.UintVar(&cmd.x, "x", 0, "X coordinate of the origin of flood")
	flags.UintVar(&cmd.y, "y", 0, "Y coordinate of the origin of flood")
	flags.StringVar(&cmd.f, "fill", "", "fill ASCII character of flood")

	return flags
}

func (cmd *floodFillCmd) Run(client pb.CanvasciiClient) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	req, err := client.FloodFill(ctx, cmd.flood())
	if err != nil {
		log.Fatalf("failed to flood fill: %v", err)
	}
	log.Printf("Successfully flood filled: %v\n", req)

	return nil
}
