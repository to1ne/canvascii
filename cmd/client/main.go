package main

import (
	"flag"
	"log"
	"os"

	pb "gitlab.com/to1ne/canvascii/canvascii"
	"google.golang.org/grpc"
)

type subCmd interface {
	Flags() *flag.FlagSet
	Run(pb.CanvasciiClient) error
}

var subCommands = []subCmd{
	&drawRectCmd{},
	&floodFillCmd{},
	&watchCmd{},
}

var address, canvasName string

func commandName() string {
	flags := flag.NewFlagSet("canvascii-client", flag.ExitOnError)
	flags.Parse(os.Args)

	if flags.NArg() < 2 {
		log.Fatalf("command not specified")
	}

	return flags.Arg(1)
}

func buildSubCommand() subCmd {
	var cmd subCmd

	name := commandName()

	for _, c := range subCommands {
		if c.Flags().Name() == name {
			cmd = c
			break
		}
	}

	if cmd == nil {
		log.Fatalf("subcommand not recognized: %s\n", name)
	}

	flags := cmd.Flags()
	flags.StringVar(&address, "address", "", "The address of the server to connect to")
	flags.StringVar(&canvasName, "canvas", "", "The name of the canvas")
	flags.Parse(os.Args[2:])

	if flags.NArg() != 0 {
		log.Fatalf("unhandled arguments for subcommand: %s\n", os.Args[1])
	}

	if address == "" {
		log.Fatalf("no address specified")
	}

	return cmd
}

func main() {
	cmd := buildSubCommand()

	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("failed to connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewCanvasciiClient(conn)

	cmd.Run(client)
}
