package main

import (
	"fmt"
	"io"
	"time"

	pb "gitlab.com/to1ne/canvascii/canvascii"
)

type LiveCanvas struct {
	width, height int
	canvas        [][]rune
	output        io.Writer
	delay         time.Duration
}

type coord struct {
	x, y int
}

func makeCanvasSlice(rows, cols int, rn rune) [][]rune {
	a := make([][]rune, cols)
	for i := range a {
		row := make([]rune, rows)
		for j := range row {
			row[j] = rn
		}
		a[i] = row
	}

	return a
}

func NewLiveCanvas(f io.Writer, width, height, delay int) LiveCanvas {
	c := LiveCanvas{
		width: width, height: height,
		canvas: makeCanvasSlice(width, height, ' '),
		output: f,
		delay:  time.Duration(delay) * time.Millisecond,
	}

	// clear the entire screen
	c.Printf("\033[3J")

	return c
}

func (c *LiveCanvas) DrawFlood(flood *pb.Flood) {
	book := makeCanvasSlice(c.width, c.height, 0)
	todo := []pb.Coordinate{*flood.Origin}
	f := rune(flood.Fill)
	o := c.canvas[flood.Origin.Y][flood.Origin.X]

	for {
		pos := todo[0]
		todo = todo[1:]

		// Punch rune when matches expected rune
		if o == c.canvas[pos.Y][pos.X] {
			c.PunchRune(int(pos.X), int(pos.Y), f)

			// Find neighbors which might need punching
			for _, n := range []pb.Coordinate{
				pb.Coordinate{X: pos.X - 1, Y: pos.Y},
				pb.Coordinate{X: pos.X, Y: pos.Y - 1},
				pb.Coordinate{X: pos.X + 1, Y: pos.Y},
				pb.Coordinate{X: pos.X, Y: pos.Y + 1}} {
				if c.inBounds(int(n.X), int(n.Y)) && book[pos.Y][pos.X] == 0 {
					todo = append(todo, n)
				}
			}
		}
		book[pos.Y][pos.X] = 1

		if len(todo) <= 0 {
			break
		}
	}
}

func (c *LiveCanvas) DrawRect(rect *pb.Rect) {
	if rect.Origin == nil {
		return
	}

	x := int(rect.Origin.X)
	y := int(rect.Origin.Y)
	h := int(rect.Height)
	w := int(rect.Width)

	if h == 0 || w == 0 {
		return
	}

	if rect.Fill != 0 {
		f := rune(rect.Fill)
		c.FillRect(x, y, h, w, f)
	}
	if rect.Outline != 0 {
		o := rune(rect.Outline)
		c.FillRect(x, y, h, 1, o)
		if h > 1 {
			c.FillRect(x, y+h-1, 1, w, o)
		}
		if w > 1 {
			c.FillRect(x, y, 1, w, o)
			c.FillRect(x+w-1, y, h, 1, o)
		}
	}
}

func (c *LiveCanvas) FillRect(x, y, h, w int, rn rune) {
	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			c.PunchRune(x+j, y+i, rn)
		}
	}
}

func (c *LiveCanvas) PunchRune(x, y int, rn rune) {
	if !c.inBounds(x, y) {
		return
	}

	c.canvas[y][x] = rn
	c.Printf("\033[%d;%dH%c", y+1, x+1, rn)

	if c.delay > 0 {
		time.Sleep(c.delay)
	}
}

func (c *LiveCanvas) Printf(format string, a ...interface{}) (int, error) {
	if c.output != nil {
		return fmt.Fprintf(c.output, format, a...)
	}

	return 0, nil
}

func (c *LiveCanvas) inBounds(x, y int) bool {
	if x < 0 || y < 0 {
		return false
	}
	if x >= c.width || y >= c.height {
		return false
	}
	return true

}
