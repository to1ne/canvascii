package main

import (
	"testing"

	"gitlab.com/to1ne/canvascii/internal/testhelper"
)

func TestDrawRectCmd_rect(t *testing.T) {
	drawRect := drawRectCmd{x: 1, y: 2, w: 3, h: 4, f: "@", o: "*"}
	expected := testhelper.NewRect(1, 2, 3, 4, 64, 42)

	testhelper.ProtoEqual(t, expected, drawRect.rect())
}
