package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"os"

	pb "gitlab.com/to1ne/canvascii/canvascii"
)

type watchCmd struct {
	width, height, delay int
}

func (cmd *watchCmd) Flags() *flag.FlagSet {
	flags := flag.NewFlagSet("watch", flag.ExitOnError)
	flags.IntVar(&cmd.width, "width", 80, "width of the canvas")
	flags.IntVar(&cmd.height, "height", 40, "height of the canvas")
	flags.IntVar(&cmd.delay, "delay", 0, "delay in ms between drawing runes")

	return flags
}

func (cmd *watchCmd) Run(client pb.CanvasciiClient) error {
	stream, err := client.Watch(context.Background(), &pb.Hello{CanvasName: canvasName})
	if err != nil {
		return fmt.Errorf("failed to open watch stream: %v", err)
	}

	canvas := NewLiveCanvas(os.Stdout, cmd.width, cmd.height, cmd.delay)

	for {
		op, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return fmt.Errorf("Error while receiving: %v", err)

		}
		switch {
		case op.GetRect() != nil:
			canvas.DrawRect(op.GetRect())
		case op.GetFlood() != nil:
			canvas.DrawFlood(op.GetFlood())
		}
	}

	return nil
}
