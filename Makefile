ADDRESS ?= localhost:5000

all: gen

gen: canvascii/canvascii.pb.go

test:
	go test ./cmd/server ./cmd/client $(if ${TEST_VERBOSE},-v) $(if ${TEST_CASES},$(addprefix -run ,${TEST_CASES}))

%.pb.go: %.proto
	protoc --go_out=. --go_opt=paths=source_relative \
	       --go-grpc_out=. --go-grpc_opt=paths=source_relative \
	$<

serve:
	go run ./cmd/server -address=${ADDRESS}

watch:
	go run ./cmd/client watch -address=${ADDRESS} $(if ${CANVAS},-canvas=${CANVAS}) $(if ${WIDTH},-width=${WIDTH}) $(if ${HEIGHT},-height=${HEIGHT}) $(if ${DELAY},-delay=${DELAY})

draw-rect:
	go run ./cmd/client draw-rect -address=${ADDRESS} $(if ${CANVAS},-canvas=${CANVAS}) $(if $X,-x=$X) $(if $Y,-y=$Y) $(if ${WIDTH},-width=${WIDTH}) $(if ${HEIGHT},-height=${HEIGHT}) $(if ${FILL},-fill=${FILL}) $(if ${OUTLINE},-outline=${OUTLINE})

flood-fill:
	go run ./cmd/client flood-fill -address=${ADDRESS} $(if ${CANVAS},-canvas=${CANVAS}) $(if $X,-x=$X) $(if $Y,-y=$Y) $(if ${FILL},-fill=${FILL})

.PHONY: all gen serve watch draw-rect flood-fill
