module gitlab.com/to1ne/canvascii

go 1.16

require (
	github.com/stretchr/testify v1.7.0 // indirect
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
)
