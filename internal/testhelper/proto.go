package testhelper

import (
	"testing"

	"github.com/stretchr/testify/require"
	pb "gitlab.com/to1ne/canvascii/canvascii"
	"google.golang.org/protobuf/proto"
)

// ProtoEqual asserts that expected and actual protobuf messages are equal.
func ProtoEqual(t testing.TB, expected proto.Message, actual proto.Message) {
	require.True(t, proto.Equal(expected, actual), "proto messages not equal\nexpected: %v\ngot:      %v", expected, actual)
}

// NewRect creates a new rect with the given parameters
func NewRect(x, y, w, h, f, o uint32) *pb.Rect {
	return &pb.Rect{Origin: &pb.Coordinate{X: x, Y: y},
		Width: w, Height: h,
		Fill: f, Outline: o}

}
